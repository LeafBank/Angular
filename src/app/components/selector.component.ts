import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'selector',
  templateUrl: './selector.component.html'
})
export class SelectorComponent {
  label = '';
  displayList = false;

  @Input() defaultLabel = 'Unknown';
  @Input() ariaLabelledBy?: string;
  @Input() options: string[] = [];
  @Output() btnClick = new EventEmitter();
  @Output() optClick = new EventEmitter();

  handleButton(event: Event) {
    this.displayList = !this.displayList;
    this.btnClick.emit(event);
  }

  handleOption(value: string) {
    this.label = value;
    this.displayList = false;
    this.optClick.emit(value);
  }
}
