import { Component, OnInit } from '@angular/core';
import { getYears, convertMonthToInt, convertYearToInt } from '../utils';
import { MONTHS, LOREM_IPSUM } from '../constants';
import { CreditCardService } from '../services/credit-card.service';

@Component({
  selector: 'app',
  templateUrl: './app.component.html'
})
export class AppComponent implements OnInit {
  reversed = false;
  cardHolder = '';
  cardNumber = '';
  months = MONTHS;
  month = '';
  years = getYears(5, 20);
  year = '';
  crypto = '';
  loading = false;
  dialogTitle = '';
  dialogVisibility = false;
  dialogError = false;

  constructor(private creditCardService: CreditCardService) {}

  ngOnInit() {
    // eslint-disable-next-line
    console.log('Leaf Bank');
  }

  setReversed(value: boolean) {
    this.reversed = value;
  }

  setCardHolder(event: Event) {
    this.cardHolder = (event.target as HTMLInputElement).value;
  }

  setCardNumber(event: Event) {
    (event.target as HTMLInputElement).value = (event.target as HTMLInputElement).value
      .replace(/[^\dA-Z]/g, '') // NOTE: Escapes Digits, Matches Words
      .replace(/(.{4})/g, '$1 ') // NOTE: Any Chars, 4 Times
      .trim();

    this.cardNumber = (event.target as HTMLInputElement).value;
  }

  getConvertedMonth(): string {
    return convertMonthToInt(this.month);
  }

  setMonth(value: string) {
    this.month = value;
  }

  getConvertedYear(): string {
    return convertYearToInt(this.year);
  }

  setYear(value: string) {
    this.year = value;
  }

  setCrypto(event: Event) {
    (event.target as HTMLInputElement).value = (event.target as HTMLInputElement).value
      .replace(/[^\dA-Z]/g, '') // NOTE: Escapes Digits, Matches Words
      .trim();

    this.crypto = (event.target as HTMLInputElement).value;
  }

  isValid(): boolean {
    if (this.cardHolder.length === 0) {
      return false;
    }

    if (this.cardNumber.length === 0) {
      return false;
    }

    if (this.month.length === 0) {
      return false;
    }

    if (this.year.length === 0) {
      return false;
    }

    if (this.crypto.length === 0) {
      return false;
    }

    return true;
  }

  handleSubmit(event: Event) {
    event.preventDefault();

    this.loading = true;

    const expirationDate = new Date();
    expirationDate.setMonth(+this.month);
    expirationDate.setFullYear(+this.year);

    this.creditCardService
      .validateCreditCard({
        cardHolder: this.cardHolder,
        cardNumber: this.cardNumber.replace(/\s/g, ''), // NOTE: Matches Whitespaces
        expirationDate,
        crypto: this.crypto
      })
      .then(res => {
        this.dialogTitle = res.message;

        setTimeout(() => {
          this.dialogVisibility = true;
        }, 0.5 * 1000);
      })
      .catch(err => {
        this.dialogError = true;
        this.dialogTitle = err.message;

        setTimeout(() => {
          this.dialogVisibility = true;
        }, 0.5 * 1000);
      })
      .finally(() => (this.loading = false));
  }
}
