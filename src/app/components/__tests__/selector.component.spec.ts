import { render, screen } from '@testing-library/angular';
import userEvent from '@testing-library/user-event';
import { SelectorComponent } from '../selector.component';

describe('SelectorComponent', () => {
  it('Should Match The Snapshot', async () => {
    const { container } = await render(SelectorComponent);

    expect(container).toMatchSnapshot();
  });

  it('Should Renders', async () => {
    await render(SelectorComponent, {
      componentProperties: { defaultLabel: 'Hello World', ariaLabelledBy: 'helloWorld', options: ['Lorem Ipsum'] }
    });

    expect(screen.queryByText('Lorem Ipsum')).not.toBeInTheDocument();
  });

  it('Should Emit Events', async () => {
    const btnClickMock = jest.fn();

    await render(SelectorComponent, {
      componentProperties: {
        defaultLabel: 'Hello World',
        ariaLabelledBy: 'helloWorld',
        options: ['Lorem Ipsum'],
        btnClick: {
          emit: btnClickMock
        } as any
      }
    });

    const buttonElement = screen.getByText('Hello World');

    userEvent.click(buttonElement);

    expect(screen.queryByText('Lorem Ipsum')).toBeInTheDocument();
    expect(btnClickMock).toHaveBeenCalled();
  });
});
