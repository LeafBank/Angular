import { render, screen, fireEvent } from '@testing-library/angular';
import userEvent from '@testing-library/user-event';
import { FieldComponent } from '../field.component';

describe('FieldComponent', () => {
  it('Should Match The Snapshot', async () => {
    const { container } = await render(FieldComponent, { componentProperties: { id: 'field' } });

    expect(container).toMatchSnapshot();
  });

  it('Should Renders', async () => {
    await render(FieldComponent, {
      componentProperties: {
        classes: 'row',
        style: 'margin: auto',
        id: 'field',
        name: 'field',
        label: 'Field',
        type: 'text',
        placeholder: 'Field',
        maxLength: 10
      }
    });

    expect(screen.queryByText('Field')).toBeInTheDocument();
    expect(screen.queryByPlaceholderText('Field')).toBeInTheDocument();
  });

  it('Should Emit Events', async () => {
    const onChangeMock = jest.fn();
    const onFocusMock = jest.fn();

    await render(FieldComponent, {
      componentProperties: {
        classes: 'row',
        style: 'margin: auto',
        id: 'field',
        name: 'field',
        label: 'Field',
        type: 'text',
        placeholder: 'Field',
        maxLength: 10,
        onInput: {
          emit: onChangeMock
        } as any,
        onFocus: {
          emit: onFocusMock
        } as any
      }
    });

    const fieldElement = screen.getByPlaceholderText('Field');

    fireEvent.focus(fieldElement);
    fireEvent.input(fieldElement, { target: { value: 'Hello World' } });

    expect(screen.queryByPlaceholderText('Field')).toHaveValue('Hello World');
    expect(onChangeMock).toHaveBeenCalled();
    expect(onFocusMock).toHaveBeenCalled();
  });
});
