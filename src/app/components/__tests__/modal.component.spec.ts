import { render } from '@testing-library/angular';
import { ModalComponent } from '../modal.component';

describe('ModalComponent', () => {
  it('Should Match The Snapshot', async () => {
    const { container } = await render(ModalComponent, {
      componentProperties: { isVisible: true, title: 'Hello World' }
    });

    expect(container).toMatchSnapshot();
  });

  it('Should Renders As Resolved', async () => {
    const { container, getByText } = await render(ModalComponent, {
      componentProperties: {
        isVisible: true,
        title: 'Hello World'
      }
    });

    const iconElement = container.querySelector('.modal-icon');

    expect(iconElement).toHaveStyle({ color: 'rgb(16, 185, 129);' });
    expect(getByText('Hello World')).toBeInTheDocument();
    expect(getByText(/Lorem ipsum dolor sit amet/)).toBeInTheDocument();
  });

  it('Should Renders As Rejected', async () => {
    const { container, getByText } = await render(ModalComponent, {
      componentProperties: {
        isVisible: true,
        isError: true,
        title: 'Hello World'
      }
    });

    const iconElement = container.querySelector('.modal-icon');

    expect(iconElement).toHaveStyle({ color: 'rgb(239, 68, 68);' });
    expect(getByText('Hello World')).toBeInTheDocument();
    expect(getByText(/Lorem ipsum dolor sit amet/)).toBeInTheDocument();
  });
});
