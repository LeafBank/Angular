import { render, fireEvent, waitFor } from '@testing-library/angular';
import userEvent from '@testing-library/user-event';
import { AppComponent } from '../app.component';
import { CardComponent, RectoComponent, VersoComponent } from '../card';
import { FieldComponent } from '../field.component';
import { ModalComponent } from '../modal.component';
import { SelectorComponent } from '../selector.component';
import { CreditCardService } from '../../services/credit-card.service';

describe('AppComponent', () => {
  const now = new Date();

  it('Should Match The Snapshot', async () => {
    const { container } = await render(AppComponent, {
      declarations: [CardComponent, RectoComponent, VersoComponent, FieldComponent, ModalComponent, SelectorComponent],
      providers: [CreditCardService]
    });

    expect(container).toMatchSnapshot();
  });

  xit('Should Submit The Form With Invalid Date', async () => {
    const { getAllByPlaceholderText, getByText, queryByText } = await render(AppComponent, {
      declarations: [CardComponent, RectoComponent, VersoComponent, FieldComponent, ModalComponent, SelectorComponent],
      providers: [CreditCardService]
    });

    const [, cardHolderElement] = getAllByPlaceholderText('John Doe');
    fireEvent.input(cardHolderElement, { target: { value: 'Doe John' } });

    const [, cardNumberElement] = getAllByPlaceholderText('5678 1234 5678 1234');
    fireEvent.input(cardNumberElement, { target: { value: '4321 1234 8765 5678' } });

    const monthButton = getByText('Month');
    userEvent.click(monthButton);

    const monthOption = getByText('December');
    userEvent.click(monthOption);

    const yearButton = getByText('Year');
    userEvent.click(yearButton);

    const yearOption = getByText(`${now.getFullYear() - 5}`);
    userEvent.click(yearOption);

    const [, cryptoElement] = getAllByPlaceholderText('567');
    fireEvent.input(cryptoElement, { target: { value: '123' } });

    const submitButton = getByText('Submit');
    userEvent.click(submitButton);

    await waitFor(() => {
      expect(queryByText('Expiration Date Exceeded')).toBeInTheDocument();
    });
  });

  xit('Should Submit The Form With Invalid Number', async () => {
    const { getAllByPlaceholderText, getByText, queryByText } = await render(AppComponent, {
      declarations: [CardComponent, RectoComponent, VersoComponent, FieldComponent, ModalComponent, SelectorComponent],
      providers: [CreditCardService]
    });

    const [, cardHolderElement] = getAllByPlaceholderText('John Doe');
    fireEvent.input(cardHolderElement, { target: { value: 'Doe John' } });

    const [, cardNumberElement] = getAllByPlaceholderText('5678 1234 5678 1234');
    fireEvent.input(cardNumberElement, { target: { value: '1234 4321 5678 8765' } });

    const monthButton = getByText('Month');
    userEvent.click(monthButton);

    const monthOption = getByText('December');
    userEvent.click(monthOption);

    const yearButton = getByText('Year');
    userEvent.click(yearButton);

    const yearOption = getByText(`${now.getFullYear() + 5}`);
    userEvent.click(yearOption);

    const [, cryptoElement] = getAllByPlaceholderText('567');
    fireEvent.input(cryptoElement, { target: { value: '123' } });

    const submitButton = getByText('Submit');
    userEvent.click(submitButton);

    await waitFor(() => {
      expect(queryByText('Unsupported Card Number')).toBeInTheDocument();
    });
  });

  xit('Should Submit The Form', async () => {
    const { getAllByPlaceholderText, getByText, queryByText } = await render(AppComponent, {
      declarations: [CardComponent, RectoComponent, VersoComponent, FieldComponent, ModalComponent, SelectorComponent],
      providers: [CreditCardService]
    });

    const [, cardHolderElement] = getAllByPlaceholderText('John Doe');
    fireEvent.input(cardHolderElement, { target: { value: 'Doe John' } });

    const [, cardNumberElement] = getAllByPlaceholderText('5678 1234 5678 1234');
    fireEvent.input(cardNumberElement, { target: { value: '4321 1234 8765 5678' } });

    const monthButton = getByText('Month');
    userEvent.click(monthButton);

    const monthOption = getByText('December');
    userEvent.click(monthOption);

    const yearButton = getByText('Year');
    userEvent.click(yearButton);

    const yearOption = getByText(`${now.getFullYear() + 5}`);
    userEvent.click(yearOption);

    const [, cryptoElement] = getAllByPlaceholderText('567');
    fireEvent.input(cryptoElement, { target: { value: '123' } });

    const submitButton = getByText('Submit');
    userEvent.click(submitButton);

    await waitFor(() => {
      expect(queryByText('Credit Card Saved')).toBeInTheDocument();
    });
  });
});
