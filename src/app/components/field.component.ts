import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'field',
  templateUrl: './field.component.html'
})
export class FieldComponent {
  @Input() classes?: string;
  @Input() style?: string;
  @Input() id = '';
  @Input() name?: string;
  @Input() label?: string;
  @Input() type = 'text';
  @Input() placeholder?: string;
  @Input() maxLength?: number;
  @Output() onInput = new EventEmitter();
  @Output() onFocus = new EventEmitter();

  handleInput(event: Event) {
    this.onInput.emit(event);
  }

  handleFocus(event: Event) {
    this.onFocus.emit(event);
  }
}
