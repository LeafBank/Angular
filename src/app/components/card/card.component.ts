import { Component, Input, Output, EventEmitter } from '@angular/core';
import { PATTERN } from '../../constants';

@Component({
  selector: 'card',
  templateUrl: './card.component.html'
})
export class CardComponent {
  @Input() reversed?: boolean;
  @Input() cardNumber = '';
  @Input() fullName?: string;
  @Input() month?: string;
  @Input() year?: string;
  @Input() crypto?: string;

  getHiddenCardNumber(): string {
    return PATTERN.replace(/\*/g, (char, idx) => {
      if ((idx >= 5 && idx <= 8) || (idx >= 15 && idx <= 18)) {
        return this.cardNumber[idx] || char;
      }
      return char;
    });
  }
}
