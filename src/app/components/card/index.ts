import { CardComponent } from './card.component';
import { RectoComponent } from './recto.component';
import { VersoComponent } from './verso.component';

export { CardComponent, RectoComponent, VersoComponent };
