import { render, screen } from '@testing-library/angular';
import { RectoComponent } from '../recto.component';

describe('RectoComponent', () => {
  it('Should Match The Snapshot', async () => {
    const { container } = await render(RectoComponent, {
      componentProperties: {
        cardNumber: '**** 1234 **** 5678',
        fullName: 'JOHN DOE',
        month: '12',
        year: '34'
      }
    });

    expect(container).toMatchSnapshot();
  });

  it('Should Renders', async () => {
    await render(RectoComponent, {
      componentProperties: {
        cardNumber: '**** 1234 **** 5678',
        fullName: 'JOHN DOE',
        month: '12',
        year: '34'
      }
    });

    expect(screen.getByText('**** 1234 **** 5678')).toBeInTheDocument();
    expect(screen.getByText('JOHN DOE')).toBeInTheDocument();
    expect(screen.getByText('12/34')).toBeInTheDocument();
  });
});
