import { render, screen } from '@testing-library/angular';
import { CardComponent } from '../card.component';
import { RectoComponent } from '../recto.component';
import { VersoComponent } from '../verso.component';

describe('CardComponent', () => {
  it('Should Match The Snapshot', async () => {
    const { container } = await render(CardComponent, {
      declarations: [RectoComponent, VersoComponent],
      componentProperties: {
        reversed: true,
        cardNumber: '4321 1234 8765 5678',
        fullName: 'JOHN DOE',
        month: '12',
        year: '34',
        crypto: '567'
      }
    });

    expect(container).toMatchSnapshot();
  });

  it('Should Renders', async () => {
    await render(CardComponent, {
      declarations: [RectoComponent, VersoComponent],
      componentProperties: {
        reversed: true,
        cardNumber: '4321 1234 8765 5678',
        fullName: 'JOHN DOE',
        month: '12',
        year: '34',
        crypto: '567'
      }
    });

    expect(screen.getAllByText('**** 1234 **** 5678')).toHaveLength(2);
    expect(screen.getAllByText('JOHN DOE')).toHaveLength(2);
    expect(screen.getAllByText('12/34')).toHaveLength(2);
    expect(screen.getByText('567')).toBeInTheDocument();
  });
});
