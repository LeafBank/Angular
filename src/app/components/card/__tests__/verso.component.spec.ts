import { render, screen } from '@testing-library/angular';
import { VersoComponent } from '../verso.component';

describe('VersoComponent', () => {
  it('Should Match The Snapshot', async () => {
    const { container } = await render(VersoComponent, {
      componentProperties: {
        cardNumber: '**** 1234 **** 5678',
        fullName: 'JOHN DOE',
        month: '12',
        year: '34',
        crypto: '567'
      }
    });

    expect(container).toMatchSnapshot();
  });

  it('Should Renders', async () => {
    await render(VersoComponent, {
      componentProperties: {
        cardNumber: '**** 1234 **** 5678',
        fullName: 'JOHN DOE',
        month: '12',
        year: '34',
        crypto: '567'
      }
    });

    expect(screen.getByText('**** 1234 **** 5678')).toBeInTheDocument();
    expect(screen.getByText('JOHN DOE')).toBeInTheDocument();
    expect(screen.getByText('12/34')).toBeInTheDocument();
    expect(screen.getByText('567')).toBeInTheDocument();
  });
});
