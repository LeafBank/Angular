import { Component, Input, Output, EventEmitter, ViewEncapsulation } from '@angular/core';
import { PATTERN } from '../../constants';

@Component({
  selector: 'verso',
  templateUrl: './verso.component.html'
  // encapsulation: ViewEncapsulation.None
})
export class VersoComponent {
  @Input() cardNumber?: string;
  @Input() fullName?: string;
  @Input() month?: string;
  @Input() year?: string;
  @Input() crypto?: string;
}
