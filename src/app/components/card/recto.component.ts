import { Component, Input, Output, EventEmitter, ViewEncapsulation } from '@angular/core';
import { PATTERN } from '../../constants';

@Component({
  selector: 'recto',
  templateUrl: './recto.component.html'
  // encapsulation: ViewEncapsulation.None
})
export class RectoComponent {
  @Input() cardNumber?: string;
  @Input() fullName?: string;
  @Input() month?: string;
  @Input() year?: string;
}
