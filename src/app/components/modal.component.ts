import { Component, Input, Output, EventEmitter, ViewEncapsulation } from '@angular/core';
import { LOREM_IPSUM } from '../constants';

@Component({
  selector: 'modal',
  templateUrl: './modal.component.html'
})
export class ModalComponent {
  @Input() isVisible = false;
  @Input() isError?: boolean;
  @Input() title?: string;
  description = LOREM_IPSUM;
}
