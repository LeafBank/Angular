import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './components/app.component';
import { CardComponent, RectoComponent, VersoComponent } from './components/card';
import { FieldComponent } from './components/field.component';
import { ModalComponent } from './components/modal.component';
import { SelectorComponent } from './components/selector.component';

import { CreditCardService } from './services/credit-card.service';

@NgModule({
  declarations: [
    AppComponent,
    CardComponent,
    RectoComponent,
    VersoComponent,
    FieldComponent,
    ModalComponent,
    SelectorComponent
  ],
  imports: [BrowserModule],
  providers: [CreditCardService],
  bootstrap: [AppComponent]
})
export class AppModule {}
