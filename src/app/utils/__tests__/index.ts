import { getYears, convertMonthToInt, convertYearToInt } from '../index';

describe('utils', () => {
  it('getYears', () => {
    expect(getYears()).toHaveLength(10);
    expect(getYears(5, 15)).toHaveLength(15);
  });

  it('convertMonthToInt', () => {
    expect(convertMonthToInt()).toEqual('MM');
    expect(convertMonthToInt('December')).toEqual('12');
  });

  it('convertYearToInt', () => {
    expect(convertYearToInt()).toEqual('YY');
    expect(convertYearToInt('2020')).toEqual('20');
  });
});
