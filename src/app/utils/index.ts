import { MONTHS } from '../constants';

/**
 * @method getYears
 * @param {number} prevRange Previous Year Range
 * @param {number} nextRange Next Year Range
 * @returns {string[]} Years Range
 */
export const getYears = (prevRange = 0, nextRange = 10): string[] => {
  const now = new Date();
  let years: string[] = [];

  for (let i = 0; i < nextRange; i++) {
    const year = `${now.getFullYear() - prevRange + i}`;
    years = [...years, year];
  }

  return years;
};

/**
 * @method convertMonthToInt
 * @param {string} month Explicit Month
 * @returns {string} Numeric Month
 */
export const convertMonthToInt = (month?: string): string => {
  if (!month) return 'MM';
  const idx = MONTHS.indexOf(month);
  return `${idx + 1}`.padStart(2, '0');
};

/**
 * @method convertYearToInt
 * @param {string} year Full Year
 * @returns {string} Truncate Year
 */
export const convertYearToInt = (year?: string): string => {
  if (!year) return 'YY';
  const yy = year.substring(2, 4);
  return yy;
};
